### * Description

# Functions to download and parse fandom data by category types

### * download_fandom_media_data() @export

#' Download a fandom media page
#'
#' The urls for the fandom media pages are stored in \code{fandom_media_urls}.
#'
#' @param url URL address of a fandom media pages (for example from
#'     \code{\link{fandom_media_urls}}).
#' @param quiet Boolean.
#'
#' @return A tibble with columns "media", "fandom", "count" and "href".
#'
#' @examples
#' \dontrun{
#' z <- download_fandom_media_data(fandom_media_urls[["movies"]])
#' z
#' }
#'
#' @export

download_fandom_media_data <- function(url, quiet = FALSE) {
    verbose <- !quiet
    if (verbose) {
        cat("Downloading data from", url, "\n")
    }
    # Download page
    page <- curl::curl_fetch_memory(url)
    content <- rawToChar(page$content)
    html <- xml2::read_html(content)
    # Extract tags
    fandoms <- rvest::html_nodes(html, xpath = "//li[a[@class='tag']]")
    z <- dplyr::bind_rows(lapply(fandoms, parse_fandom))
    # Extract media type
    media <- rvest::html_text(rvest::html_node(html, css = "h2.heading"))
    if (length(media) != 1) {
        stop("Error while trying to parse media type from the page. Has the html format changed on AO3 recently?")
    }
    media <- trimws(strsplit(media, "Fandoms > ")[[1]][2])
    z$media <- media
    return(z[, c("media", "fandom", "count", "href")])
}

### * parse_fandom_node

#' Parse an html "li" node containing information about a fandom
#'
#' @param li_node Html node containing a link and text about a fandom.
#'
#' @return A one-row tibble with columns "fandom", "count" and "href".
#'

parse_fandom <- function(li_node) {
    fandom <- rvest::html_children(li_node)
    stopifnot(length(fandom) == 1)
    fandom <- fandom[[1]]
    count <- strsplit(rvest::html_text(li_node), "\n")[[1]]
    count <- count[grepl("^[ ]*[(][0-9]*[)]$", count)]
    stopifnot(length(count) == 1)
    count <- gsub("[()]", "", gsub(" ", "", count))
    count <- as.numeric(count)
    href <- rvest::html_attr(fandom, "href")
    fandom <- rvest::html_text(fandom)
    return(tibble::tibble(fandom = fandom, count = count, href = href))
}
