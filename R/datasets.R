### * Description

# Documentation for datasets shipped with the package.

### * fandom_media_urls

#' A list containing the urls to the fandom media pages
#'
#' Each url points to a page of AO3 containing the list of fandoms for a given
#' media type.
#'
#' Last update: 2020-06-13.
"fandom_media_urls"
