[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/roa3/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/roa3/commits/master)
[![Coverage report](https://gitlab.com/matthieu-bruneaux/roa3/badges/master/coverage.svg)](https://matthieu-bruneaux.gitlab.io/roa3/coverage/coverage.html)
[![R_CMD_CHECK](https://matthieu-bruneaux.gitlab.io/roa3/R-CMD-check_badge.svg)](https://matthieu-bruneaux.gitlab.io/roa3/R-CMD-check_output.txt)
[![Lifecycle Status](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/)

# An R package to download and parse data from the Archive of Our Own (AO3)

The `rao3` package contains tools to download and parse data from the non-profit website Archive of Our Own which hosts fanworks (https://archiveofourown.org/).

